<?php
/*
Plugin Name: WPezToolbox - Bundle: Cache
Plugin URI: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-cache
Description: CCH - A WPezToolbox bundle of WordPress cache plugins. Note: Only free plugins are listed here.
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleCache;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__ . '\plugins' );


function plugins( $arr_in = [] ) {

	$str_bundle           = __( 'CCH', 'wpez_tbox' );
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [

		'cache-enabler' =>
			[
				'name'     => $str_prefix . 'Cache Enabler',
				'slug'     => 'cache-enabler',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'KeyCDN',
						'url_by'  => 'https://www.keycdn.com/',
						'desc'    => 'Creates static HTML files and stores them on the servers disk. The web server will deliver the static HTML file and avoids the resource intensive backend processes (core, plugins and database). This WordPress cache engine will improve the performance of your website.',
						'url_img' => 'https://ps.w.org/cache-enabler/assets/icon-128x128.png?rev=1270483'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/cache-enabler/',
						'url_repo'   => 'https://github.com/keycdn/cache-enabler',
						'url_site'   => 'https://www.keycdn.com/',
						'url_tw'     => 'https://twitter.com/keycdn',
						'url_fb'     => 'https://www.facebook.com/keycdn'
					]
				]
			],

		'litespeed-cache' =>
			[
				'name'     => $str_prefix . 'LiteSpeed Cache',
				'slug'     => 'litespeed-cache',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'LiteSpeed Technologies',
						'url_by'  => 'https://taylorlovett.com/',
						'desc'    => 'Simple Cache was constructed after getting frustrated with the major caching plugins available and building sites with developer-only complex caching solutions that get millions of page views per day.',
						'url_img' => 'https://ps.w.org/litespeed-cache/assets/icon-128x128.png?rev=1574145'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/litespeed-cache/',
						'url_repo'   => 'https://github.com/litespeedtech/lscache_wp',
						'url_site'   => 'https://www.litespeedtech.com/products/cache-plugins/wordpress-acceleration'
					]
				]
			],

		'simple-cache' =>
			[
				'name'     => $str_prefix . 'Simple Cache',
				'slug'     => 'simple-cache',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Taylor Lovett',
						'url_by'  => 'https://taylorlovett.com/',
						'desc'    => 'Simple Cache was constructed after getting frustrated with the major caching plugins available and building sites with developer-only complex caching solutions that get millions of page views per day.',
						'url_img' => 'https://ps.w.org/simple-cache/assets/icon-128x128.png?rev=1389527'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/simple-cache/',
						'url_repo'   => 'https://github.com/tlovett1/simple-cache',
					]
				]
			],

		'swift-performance-lite' =>
			[
				'name'     => $str_prefix . 'Swift Performance Lite',
				'slug'     => 'swift-performance-lite',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'SWTE',
						'by_alt'  => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/swte/',
						'desc'    => 'Get a complete Cache & Performance plugin for free or as a pro. Swift Performance will increase the loading speed of any WordPress website and provides an intelligent, modern caching system.',
						'url_img' => 'https://ps.w.org/swift-performance-lite/assets/icon-128x128.jpg?rev=1816165'
					],
					'resources' => [
						'url_wp_org'  => 'https://wordpress.org/plugins/swift-performance-lite/',
						'url_site'    => 'https://swiftperformance.io/',
						'url_premium' => 'https://swiftperformance.io/features/',
					]
				]
			],

		'w3-total-cache' =>
			[
				'name'     => $str_prefix . 'W3 Total Cache',
				'slug'     => 'w3-total-cache',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Frederick Townes',
						'by_alt'  => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/fredericktownes/',
						'desc'    => 'Improves the SEO and user experience of your site by increasing website performance, reducing load times via features like content delivery network (CDN) integration and the latest best practices.',
						'url_img' => 'https://ps.w.org/w3-total-cache/assets/icon-128x128.png?rev=1041806'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/w3-total-cache/',
						'url_site'   => 'https://www.w3-edge.com/products/w3-total-cache/',
					]
				]
			],

		'wp-rocket'                       =>
			[
				'name'     => $str_prefix . 'WP Rocket',
				'slug'     => 'wp-rocket',
				'source'   => 'https://github.com/wp-media/wp-rocket',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'WP Media',
						'by_alt'  => 'GitHub Profile',
						'url_by'  => 'https://github.com/wp-media',
						'desc'    => 'We aim to help make the web faster, one WordPress website at a time. That’s why we created WP Rocket. It\'s a caching plugin that simplifies the process and helps decrease a website’s load time.',
						'url_img' => 'https://avatars1.githubusercontent.com/u/12661344?s=200&v=4',
					],
					'resources' => [
						'url_repo'    => 'https://github.com/wp-media/wp-rocket',
						'url_site'    => 'https://wp-rocket.me/',
						'url_premium'    => 'https://wp-rocket.me/pricing/',
						'url_fb'      => 'https://www.facebook.com/pages/WP-Rocket/631942253526829',
						'url_tw'      => 'https://twitter.com/wp_rocket',
					],
				],
			],


		'wp-super-cache' =>
			[
				'name'     => $str_prefix . 'WP Super Cache',
				'slug'     => 'wp-super-cache',
				'required' => false,
				'wpez'     => [
					'info'      => [
						'by'      => 'Automattic',
						'by_alt'  => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/automattic/',
						'desc'    => 'Generates static html files from your dynamic WordPress blog. After a html file is generated your webserver will serve that file instead of processing the comparatively heavier and more expensive WordPress PHP scripts.',
						'url_img' => 'https://ps.w.org/wp-super-cache/assets/icon-128x128.png?rev=1095422'
					],
					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/wp-super-cache/',
						'url_repo'   => 'https://github.com/Automattic/wp-super-cache',
					]
				]
			],
	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}